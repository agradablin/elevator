#include "stdafx.h"
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <vector> 
#include <iterator>
#include <windows.h>

using namespace std;

string sInfo = "Rule: f[n] - call on floor, e[n] -  elevator button, q - exit.";

enum LiftCondition { moving, opening, closing, waiting };
enum AlgOperation { add, del, get };


class LiftAlgorythm
{
	size_t m_iFloorsCount, m_iCurFloor = 1;
	std::vector<size_t> m_qTasks;
	std::mutex m_lock;
	size_t WorkWithQue(AlgOperation eOperation, size_t floor)
	{
		m_lock.lock();
		switch (eOperation)
		{
			case AlgOperation::add:
			{
				size_t iTempCurFloor = m_iCurFloor;
				bool bFoundPlace = false;
				for (std::vector<size_t>::iterator it = m_qTasks.begin(); it != m_qTasks.end(); ++it)
				{
					if ((floor < iTempCurFloor && floor > *it) || (floor > iTempCurFloor && floor < *it))
					{
						m_qTasks.insert(it, floor);
						bFoundPlace = true;
						break;
					}
					iTempCurFloor = *it;
				}
				if (!bFoundPlace)
					m_qTasks.push_back(floor);
				break;
			}
			case AlgOperation::del:
			{
				m_iCurFloor = floor;
				for (std::vector<size_t>::iterator it = m_qTasks.begin(); it != m_qTasks.end(); ++it)
					if (*it == floor)
					{
						m_qTasks.erase(it);			//найти и удалить, он может быть не первым
						break;
					}
				break;
			}
		}
		m_lock.unlock();
		if (m_qTasks.size() > 0)
			return m_qTasks.front();
		else
			return 0;
	}

public:
	LiftAlgorythm(size_t floors) : m_iFloorsCount(floors) {}
	size_t MaxFloor() const { return m_iFloorsCount; }
	size_t GetNextFloor() { return WorkWithQue(AlgOperation::get, 0); }
	void AddFloor(size_t& floor) { WorkWithQue(AlgOperation::add, floor); }
	void FloorArrived(size_t& floor) { WorkWithQue(AlgOperation::del, floor);  }
};


class LiftMechanics
{
	LiftAlgorythm *m_pAlg;
	size_t m_iTarget = 0, m_iCurFloor = 1;
	size_t m_iDoorTime;
	float m_fFloorTime;

	bool m_bLetsStart = false, m_bTerminate = false;
	DWORD m_dwOperationTime;
	LiftCondition m_eCondition = LiftCondition::waiting;

	thread m_thread;

	void CloseDoors()
	{
		cout << " Closing doors !" << endl;
		m_dwOperationTime = GetTickCount() + m_iDoorTime;
		m_eCondition = LiftCondition::closing;
	}

	void OpenDoors()
	{
		cout << " Opening doors! " << endl;
		m_dwOperationTime = GetTickCount() + m_iDoorTime;
		m_eCondition = LiftCondition::opening;
	}
	void CheckOnFloor()
	{
		if (m_iCurFloor < m_iTarget)
			++m_iCurFloor;
		else
			if (m_iCurFloor > m_iTarget)
				--m_iCurFloor;
		cout << m_iCurFloor << " ";
		m_iTarget = m_pAlg->GetNextFloor();  //берем следующий таргет пока не стерли
		m_pAlg->FloorArrived(m_iCurFloor);   //стираем
		if (m_iTarget != 0)
			if (m_iCurFloor == m_iTarget)
			{
				OpenDoors();
			}
			else
				Move();
	}
	
	void Move()
	{
		m_iTarget = m_pAlg->GetNextFloor();
		if (m_iTarget != 0)
		{
			if (m_iCurFloor != m_iTarget)
			{
				m_bLetsStart = false;
				//cout << " Going to " << m_iTarget << ": ";
				m_dwOperationTime = GetTickCount() + m_fFloorTime;
				m_eCondition = LiftCondition::moving;
			}
			else
			{
				CheckOnFloor();
			}
		}
		else
		{
			//cout << " Nothing to do... ";
			m_eCondition = LiftCondition::waiting;
		}
	}
	
	void Start() 
	{
		while (!m_bTerminate)
		{
			if (m_eCondition != LiftCondition::waiting)
			{
				int iDiff = m_dwOperationTime - GetTickCount();
				if (iDiff <= 0)
				{
					switch (m_eCondition)
					{
					case LiftCondition::closing:
						Move();
						break;
					case LiftCondition::opening:
						CloseDoors();
						break;
					case LiftCondition::moving:
						CheckOnFloor();
						break;
					}
				}
			}
			else
				if (m_bLetsStart)
				{
					Move();
				}
		}
		cout << endl << "Elevator stopped!" << endl;
	}

public:
	LiftMechanics(size_t doorTime, float timeForFloor, LiftAlgorythm* pAlgo) : m_iDoorTime(doorTime), m_fFloorTime(timeForFloor), m_pAlg(pAlgo) {}
//	void SetNextFloor(const size_t& floor) { m_iTarget = floor; }
	void Terminate() { m_bTerminate = true; }
	~LiftMechanics() {
		if (m_thread.joinable())
			m_thread.join();
	}
	void runLift() { m_thread = thread(&LiftMechanics::Start, this); }
	void StartEngine() {
		if (m_eCondition == LiftCondition::waiting) 
			m_bLetsStart = true; 
	};
};


class LiftProcessor
{
	LiftAlgorythm* m_pAlg;
	LiftMechanics* m_pLift;
public:
	LiftProcessor(LiftAlgorythm* LA, LiftMechanics* LM) : m_pAlg(LA), m_pLift(LM) {}
	void onButtion(size_t& floor) const 
	{	m_pAlg->AddFloor(floor); 
		m_pLift->StartEngine();
	}
	size_t MaxFloor() const { return m_pAlg->MaxFloor(); }
	void RunLift() { m_pLift->runLift(); }
	void StopLift() { m_pLift->Terminate(); }
};



void DoTheElevator(LiftProcessor* Liftus)
{
	//string sInfo = "Rule: f[n] - call on floor, e[n] -  elevator button, q - exit.";
	Liftus->RunLift();
	cout << " Elevator is on! " << sInfo << endl;
	while (true) {
		string str;
		cin >> str;
		if (str == "q") {
			Liftus->StopLift();
			break;
		}
		else
		{
			size_t iTask = 0;
			try {
				if (str.length() > 1)
					iTask = stoi(str.substr(1, str.length() - 1));
			}
			catch (...) {}
			if (iTask > 0 && iTask <= Liftus->MaxFloor() && (str[0] == 'e' || str[0] == 'f'))
			{
				Liftus->onButtion(iTask);
			}
			else
				cout << "Error: bad command! " << sInfo << endl;
		}
	}
}



int main(int argc, char* argv[]) {

	int iFloors, iDoorTime;
	float fSpeed, fHeight;

	if (argc > 3)	{
		iFloors = atoi(argv[1]);
		fHeight = atof(argv[2]);
		fSpeed = atof(argv[3]);
		iDoorTime = atoi(argv[4]);
		
		if (fHeight > 0 && fSpeed > 0 && (iFloors > 4 && iFloors < 21)) {
			auto pAlgo = new LiftAlgorythm(iFloors);
			auto pMechs = new LiftMechanics(iDoorTime * 1000, (fHeight / fSpeed) * 1000, pAlgo);
			DoTheElevator( new LiftProcessor(pAlgo, pMechs) );
		}
		else
		{
			cout << "Your arguments are invalid. (" << endl;
		}
	}
	else
		cout << "Using: elevator [floors (5 to 20)] [floor height (m)] [elevator speed (m/s)] [door open-close time (s)]" << endl;

	return 0;
}


/*
void ShowQue() {
cout << endl << "Que: ";
copy( qTasks.begin(), qTasks.end(), ostream_iterator<int>(std::cout, ", ") );
cout << endl;
}

*/

